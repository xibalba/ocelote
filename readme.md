# Ocelote Utility Package

Light and standalone utility package for general purpose, with syntactic sugar and OO wrappers.

# Ocelote: Paquete de utilerías

Ocelore es un paquete PHP de utiliterías, azúcar sintáctico y envolventes OO para funciones de uso común.

.. code-block:: php

	// Verificar si una variable está vacía
	if(Checker::isEmpty($variable)) {
		// Hacer algo...
	}

	// Convertir una cadena json en un arreglo
	$data = Converter::toArray($someJson);

	// Explotar una cadena
	$strAsArray = StringHelper::explode($someString, ",");

¡Y mucho más!

[Leer Documentación](https://ocelote.rtfd.io)
