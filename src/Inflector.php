<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote;

/**
 * String and grammar inflection. Converts strings to a certain format. Camel cased, singular, plural etc.
 *
 * This is and adaptation of the Titon and Yii Inflector class
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 *
 * @package xibalba\ocelote
 */
class Inflector {
	/**
	 * @var array the rules for converting a word into its plural form.
	 * The keys are the regular expressions and the values are the corresponding replacements.
	 */
	public static $plurals = [
		'/([nrlm]ese|deer|fish|sheep|measles|ois|pox|media)$/i' => '\1',
		'/^(sea[- ]bass)$/i' => '\1',
		'/(m)ove$/i' => '\1oves',
		'/(f)oot$/i' => '\1eet',
		'/(h)uman$/i' => '\1umans',
		'/(s)tatus$/i' => '\1tatuses',
		'/(s)taff$/i' => '\1taff',
		'/(t)ooth$/i' => '\1eeth',
		'/(quiz)$/i' => '\1zes',
		'/^(ox)$/i' => '\1\2en',
		'/([m|l])ouse$/i' => '\1ice',
		'/(matr|vert|ind)(ix|ex)$/i' => '\1ices',
		'/(x|ch|ss|sh)$/i' => '\1es',
		'/([^aeiouy]|qu)y$/i' => '\1ies',
		'/(hive)$/i' => '\1s',
		'/(?:([^f])fe|([lr])f)$/i' => '\1\2ves',
		'/sis$/i' => 'ses',
		'/([ti])um$/i' => '\1a',
		'/(p)erson$/i' => '\1eople',
		'/(m)an$/i' => '\1en',
		'/(c)hild$/i' => '\1hildren',
		'/(buffal|tomat|potat|ech|her|vet)o$/i' => '\1oes',
		'/(alumn|bacill|cact|foc|fung|nucle|radi|stimul|syllab|termin|vir)us$/i' => '\1i',
		'/us$/i' => 'uses',
		'/(alias)$/i' => '\1es',
		'/(ax|cris|test)is$/i' => '\1es',
		'/s$/' => 's',
		'/^$/' => '',
		'/$/' => 's',
	];

	/**
	 * @var array the rules for converting a word into its singular form.
	 * The keys are the regular expressions and the values are the corresponding replacements.
	 */
	public static $singulars = [
		'/([nrlm]ese|deer|fish|sheep|measles|ois|pox|media|ss)$/i' => '\1',
		'/^(sea[- ]bass)$/i' => '\1',
		'/(s)tatuses$/i' => '\1tatus',
		'/(f)eet$/i' => '\1oot',
		'/(t)eeth$/i' => '\1ooth',
		'/^(.*)(menu)s$/i' => '\1\2',
		'/(quiz)zes$/i' => '\\1',
		'/(matr)ices$/i' => '\1ix',
		'/(vert|ind)ices$/i' => '\1ex',
		'/^(ox)en/i' => '\1',
		'/(alias)(es)*$/i' => '\1',
		'/(alumn|bacill|cact|foc|fung|nucle|radi|stimul|syllab|termin|viri?)i$/i' => '\1us',
		'/([ftw]ax)es/i' => '\1',
		'/(cris|ax|test)es$/i' => '\1is',
		'/(shoe|slave)s$/i' => '\1',
		'/(o)es$/i' => '\1',
		'/ouses$/' => 'ouse',
		'/([^a])uses$/' => '\1us',
		'/([m|l])ice$/i' => '\1ouse',
		'/(x|ch|ss|sh)es$/i' => '\1',
		'/(m)ovies$/i' => '\1\2ovie',
		'/(s)eries$/i' => '\1\2eries',
		'/([^aeiouy]|qu)ies$/i' => '\1y',
		'/([lr])ves$/i' => '\1f',
		'/(tive)s$/i' => '\1',
		'/(hive)s$/i' => '\1',
		'/(drive)s$/i' => '\1',
		'/([^fo])ves$/i' => '\1fe',
		'/(^analy)ses$/i' => '\1sis',
		'/(analy|diagno|^ba|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\1\2sis',
		'/([ti])a$/i' => '\1um',
		'/(p)eople$/i' => '\1\2erson',
		'/(m)en$/i' => '\1an',
		'/(c)hildren$/i' => '\1\2hild',
		'/(n)ews$/i' => '\1\2ews',
		'/eaus$/' => 'eau',
		'/^(.*us)$/' => '\\1',
		'/s$/i' => '',
	];

	/**
	 * @var array the special rules for converting a word between its plural form and singular form.
	 * The keys are the special words in singular form, and the values are the corresponding plural form.
	 */
	public static $specials = [
		'atlas' => 'atlases',
		'beef' => 'beefs',
		'brother' => 'brothers',
		'cafe' => 'cafes',
		'child' => 'children',
		'cookie' => 'cookies',
		'corpus' => 'corpuses',
		'cow' => 'cows',
		'curve' => 'curves',
		'foe' => 'foes',
		'ganglion' => 'ganglions',
		'genie' => 'genies',
		'genus' => 'genera',
		'graffito' => 'graffiti',
		'hoof' => 'hoofs',
		'loaf' => 'loaves',
		'man' => 'men',
		'money' => 'monies',
		'mongoose' => 'mongooses',
		'move' => 'moves',
		'mythos' => 'mythoi',
		'niche' => 'niches',
		'numen' => 'numina',
		'occiput' => 'occiputs',
		'octopus' => 'octopuses',
		'opus' => 'opuses',
		'ox' => 'oxen',
		'penis' => 'penises',
		'sex' => 'sexes',
		'soliloquy' => 'soliloquies',
		'testis' => 'testes',
		'trilby' => 'trilbys',
		'turf' => 'turfs',
		'wave' => 'waves',
		'Amoyese' => 'Amoyese',
		'bison' => 'bison',
		'Borghese' => 'Borghese',
		'bream' => 'bream',
		'breeches' => 'breeches',
		'britches' => 'britches',
		'buffalo' => 'buffalo',
		'cantus' => 'cantus',
		'carp' => 'carp',
		'chassis' => 'chassis',
		'clippers' => 'clippers',
		'cod' => 'cod',
		'coitus' => 'coitus',
		'Congoese' => 'Congoese',
		'contretemps' => 'contretemps',
		'corps' => 'corps',
		'debris' => 'debris',
		'diabetes' => 'diabetes',
		'djinn' => 'djinn',
		'eland' => 'eland',
		'elk' => 'elk',
		'equipment' => 'equipment',
		'Faroese' => 'Faroese',
		'flounder' => 'flounder',
		'Foochowese' => 'Foochowese',
		'gallows' => 'gallows',
		'Genevese' => 'Genevese',
		'Genoese' => 'Genoese',
		'Gilbertese' => 'Gilbertese',
		'graffiti' => 'graffiti',
		'headquarters' => 'headquarters',
		'herpes' => 'herpes',
		'hijinks' => 'hijinks',
		'Hottentotese' => 'Hottentotese',
		'information' => 'information',
		'innings' => 'innings',
		'jackanapes' => 'jackanapes',
		'Kiplingese' => 'Kiplingese',
		'Kongoese' => 'Kongoese',
		'Lucchese' => 'Lucchese',
		'mackerel' => 'mackerel',
		'Maltese' => 'Maltese',
		'mews' => 'mews',
		'moose' => 'moose',
		'mumps' => 'mumps',
		'Nankingese' => 'Nankingese',
		'news' => 'news',
		'nexus' => 'nexus',
		'Niasese' => 'Niasese',
		'Pekingese' => 'Pekingese',
		'Piedmontese' => 'Piedmontese',
		'pincers' => 'pincers',
		'Pistoiese' => 'Pistoiese',
		'pliers' => 'pliers',
		'Portuguese' => 'Portuguese',
		'proceedings' => 'proceedings',
		'rabies' => 'rabies',
		'rice' => 'rice',
		'rhinoceros' => 'rhinoceros',
		'salmon' => 'salmon',
		'Sarawakese' => 'Sarawakese',
		'scissors' => 'scissors',
		'series' => 'series',
		'Shavese' => 'Shavese',
		'shears' => 'shears',
		'siemens' => 'siemens',
		'species' => 'species',
		'swine' => 'swine',
		'testes' => 'testes',
		'trousers' => 'trousers',
		'trout' => 'trout',
		'tuna' => 'tuna',
		'Vermontese' => 'Vermontese',
		'Wenchowese' => 'Wenchowese',
		'whiting' => 'whiting',
		'wildebeest' => 'wildebeest',
		'Yengeese' => 'Yengeese',
	];

	/**
	 * @var array fallback map for transliteration used by [[slug()]] when intl isn't available.
	 */
	public static $transliteration = [
		'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
		'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
		'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
		'ß' => 'ss',
		'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
		'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
		'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
		'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
		'ÿ' => 'y',
	];

	/**
	 * @var mixed Either a [[Transliterator]] or a string from which a [[Transliterator]]
	 * can be built for transliteration used by [[slug()]] when intl is available.
	 * @see http://php.net/manual/en/transliterator.transliterate.php
	 */
	public static $transliterator = 'Any-Latin; NFKD';

	/**
	 * Returns given word as CamelCased
	 * Converts a word like "send_email" to "SendEmail". It
	 * will remove non alphanumeric character from the word, so
	 * "who's online" will be converted to "WhoSOnline"
	 * @see variablize()
	 * @param string $word the word to CamelCase
	 * @return string
	 */
	public static function camelize(string $word) : string {
		return str_replace(' ', '', ucwords(preg_replace('/[^A-Za-z0-9]+/', ' ', $word)));
	}

	/**
	 * Inflect a string to a usual class name, Singular camel cased form.
	 * For example, converts "people" to "Person"
	 * @param string $tableName
	 * @return string
	 */
	public static function classify(string $tableName) : string {
		return static::camelize(static::singularize($tableName));
	}

	/**
	 * Inflect a word for a filename. Studly cased and capitalized.
	 *
	 * @param string $string
	 * @param string $ext
	 * @param bool $capitalize
	 * @return string
	 */
	public static function filenamize(string $string, string $ext = 'php', bool $capitalize = true) : string {
		if(mb_strpos($string, '.') !== false) $string = mb_substr($string, 0, mb_strrpos($string, '.'));

		$path = static::camelize($string);

		if(!$capitalize) $path = lcfirst($path);
		if(mb_substr($path, -(mb_strlen($ext) + 1)) !== '.' . $ext) $path .= '.' . $ext;

		return $path;
	}

	/**
	 * @return boolean if intl extension is loaded
	 */
	protected static function hasIntl() : bool {
		return extension_loaded('intl');
	}

	/**
	 * Inflect a word by replacing spaces with dashes.
	 *
	 * @param string $string
	 * @return string
	 */
	public static function hyphenate(string $string) : string {
		return str_replace(' ', '-', preg_replace('/\s{2,}+/', ' ', $string));
	}

	/**
	 * Returns a human-readable string from $word
	 * @param string $word the string to humanize
	 * @param boolean $ucAll whether to set all words to uppercase or not
	 * @return string
	 */
	public static function humanize(string $word, bool $ucAll = false) : string {
		$word = str_replace('_', ' ', preg_replace('/_id$/', '', $word));
		return $ucAll ? ucwords($word) : ucfirst($word);
	}

	/**
	 * Converts number to its ordinal English form. For example, converts 13 to 13th, 2 to 2nd ...
	 * @param integer $number the number to get its ordinal value
	 * @return string
	 */
	public static function ordinalize(int $number) : string {
		if (in_array(($number % 100), range(11, 13))) return $number . 'th';

		switch ($number % 10) {
			case 1:
				return $number . 'st';
			case 2:
				return $number . 'nd';
			case 3:
				return $number . 'rd';
			default:
				return $number . 'th';
		}
	}

	/**
	 * Inflect a string to its pluralized form.
	 *
	 * @param string $string
	 * @return string
	 */
	public static function pluralize(string $string) : string {
		if (isset(static::$specials[$string])) return static::$specials[$string];

		foreach (static::$plurals as $rule => $replacement) {
			if (preg_match($rule, $string)) return preg_replace($rule, $replacement, $string);
		}

		return $string;
	}

	/**
	 * Inflect a string to its singular form.
	 *
	 * @param string $word
	 * @return string
	 */
	public static function singularize(string $word) : string {
		$result = array_search($word, static::$specials, true);
		if($result !== false) return $result;

		foreach(static::$singulars as $rule => $replacement) {
			if (preg_match($rule, $word)) return preg_replace($rule, $replacement, $word);
		}

		return $word;
	}

	/**
	 * Returns a string with all spaces converted to given replacement,
	 * non word characters removed and the rest of characters transliterated.
	 *
	 * If intl extension isn't available uses fallback that converts latin characters only
	 * and removes the rest. You may customize characters map via $transliteration property
	 * of the helper.
	 *
	 * @param string $string An arbitrary string to convert
	 * @param string $replacement The replacement to use for spaces
	 * @param bool $lowercase whether to return the string in lowercase or not. Defaults to `true`.
	 * @return string The converted string.
	 */
	public static function slug(string $string, string $replacement = '-', bool $lowercase = true) : string {
		$string = static::transliterate($string);
		$string = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $string);
		$string = preg_replace('/[=\s—–-]+/u', $replacement, $string);
		$string = trim($string, $replacement);

		return $lowercase ? strtolower($string) : $string;
	}

	/**
	 * Alias for underscore.
	 *
	 * @param string $string
	 * @return string
	 */
	public static function snakeize(string $string) : string {
		return static::underscore($string);
	}

	/**
	 * Converts a class name to its table name (pluralized)
	 * naming conventions. For example, converts "Person" to "people"
	 * @param string $className the class name for getting related table_name
	 * @return string
	 */
	public static function tableize(string $className) : string {
		return static::pluralize(static::underscore($className));
	}

	/**
	 * Converts an underscored or CamelCase word into a English
	 * sentence.
	 * @param string $words
	 * @param bool $ucAll whether to set all words to uppercase
	 * @return string
	 */
	public static function titleize(string $words, bool $ucAll = false) : string {
		$words = static::humanize(static::underscore($words), $ucAll);
		return $ucAll ? ucwords($words) : ucfirst($words);
	}

	/**
	 * Returns transliterated version of a string.
	 *
	 * If intl extension isn't available uses fallback that converts latin characters only
	 * and removes the rest. You may customize characters map via $transliteration property
	 * of the helper.
	 *
	 * @param string $string input string
	 * @return string
	 */
	protected static function transliterate(string $string) : string {
		if(static::hasIntl()) return transliterator_transliterate(static::$transliterator, $string);
		else return str_replace(array_keys(static::$transliteration), static::$transliteration, $string);
	}

	/**
	 * Converts any "CamelCased" into an "underscored_word".
	 * @param string $words the word(s) to underscore
	 * @return string
	 */
	public static function underscore(string $string) : string {
		return strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $string));
	}

	/**
	 * Inflect a word to be used as a PHP variable. Strip all but letters, numbers and underscores.
	 * Add an underscore if the first letter is numeric.
	 *
	 * @param string $string
	 * @return string
	 */
	public static function variablize(string $string) : string {
		$string = preg_replace('/[^_a-z0-9]+/i', '', $string);
		if (is_numeric(mb_substr($string, 0, 1))) $string = '_' . $string;
		return $string;
	}
}
