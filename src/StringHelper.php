<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote;

/**
 * This class provide methods for string manipulation.
 * Also wraps php string manipulation function for Syntactic sugar pupose.
 *
 * Author note: This class is an adaptation (almost a copy-paste) of BaseStringHelper of Yii2 Framework.
 * and Utility/Str of Titon Framework.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package alpaca\ocelote
 */
class StringHelper {
	/**
	 * Returns the trailing name component of a path.
	 * This method is similar to the php function `basename()` except that it will
	 * treat both \ and / as directory separators, independent of the operating system.
	 * This method was mainly created to work on php namespaces. When working with real
	 * file paths, php's `basename()` should work fine for you.
	 * Note: this method is not aware of the actual filesystem, or path components such as "..".
	 *
	 * @param string $path A path string.
	 * @param string $suffix If the name component ends in suffix this will also be cut off.
	 * @return string the trailing name component of the given path.
	 * @see http://www.php.net/manual/en/function.basename.php
	 */
	public static function basename(string $path, string $suffix = '') : string {
		if(($len = mb_strlen($suffix)) > 0 && mb_substr($path, -$len) == $suffix) $path = mb_substr($path, 0, -$len);
		$path = rtrim(str_replace('\\', '/', $path), '/\\');
		if (($pos = mb_strrpos($path, '/')) !== false) return mb_substr($path, $pos + 1);
		return $path;
	}

	/**
	 * Returns the number of bytes in the given string.
	 * This method ensures the string is treated as a byte array by using `mb_strlen()`.
	 * @param string $string the string being measured for length
	 * @return integer the number of bytes in the given string.
	 */
	public static function byteLength(string $string) : int {
		return mb_strlen($string, '8bit');
	}

    /**
     * Returns the portion of string specified by the start and length parameters.
     * This method ensures the string is treated as a byte array by using `mb_substr()`.
     * @param string $string the input string. Must be one character or longer.
     * @param integer $start the starting position
     * @param int|null $length the desired portion length. If not specified or `null`, there will be
     * no limit on length i.e. the output will be until the end of the string.
     * @return string the extracted part of string, or FALSE on failure or an empty string.
     * @see http://www.php.net/manual/en/function.substr.php
     */
	public static function byteSubstr(string $string, int $start, ?int $length = null) : string {
		return mb_substr($string, $start, $length === null ? mb_strlen($string, '8bit') : $length, '8bit');
	}

	/**
	 * Checks to see if the passed $needle exist inside the passed string.
	 *
	 * @param string $string
	 * @param string $needle
	 * @offsot If specified, search will start this number of characters counted from the beginning of the string.
	 *
	 * @return bool
	 */
	public static function contains(string $string, string $needle, int $offset = 0, bool $strict = true) : bool {
		if(static::indexOf($string, $needle, $offset, $strict) === false) return false;
		else return true;
	}

	/**
	 * Counts words in a string.
	 *
	 * @param string $string
	 * @return int
	 */
	public static function countWords(string $string) : int {
		return count(preg_split('/\s+/u', $string, -1, PREG_SPLIT_NO_EMPTY));
	}

	/**
	 * Check if given string ends with specified substring.
	 *
	 * @param string $string
	 * @param string $needle
	 * @param bool $strict
	 * @return boolean Returns true if first input ends with second input, false otherwise
	 */
	public static function endsWith(string $string, string $needle, bool $strict = true) : bool {
		$ends = static::extract($string, -mb_strlen($needle));
		if ($strict) return ($ends === $needle);
		return (mb_strtolower($ends) === mb_strtolower($needle));
	}

	/**
	 * Returns parent directory's path.
	 * This method is similar to `dirname()` except that it will treat
	 * both \ and / as directory separators, independent of the operating system.
	 *
	 * @param string $path A path string.
	 * @return string the parent directory's path.
	 * @see http://www.php.net/manual/en/function.basename.php
	 */
	public static function dirname(string $path) : string {
		$pos = mb_strrpos(str_replace('\\', '/', $path), '/');
		if ($pos !== false) return mb_substr($path, 0, $pos);
		return '';
	}

	/**
	 * Explodes string into array, optionally trims values and skips empty ones
	 *
	 * @param string $string String to be exploded.
	 * @param string $delimiter Delimiter. Default is ','.
	 * @param mixed $trim Whether to trim each element. Can be:
	 * - boolean - to trim normally;
	 * - string - custom characters to trim. Will be passed as a second argument to `trim()` function.
	 * - callable - will be called for each value instead of trim. Takes the only argument - value.
	 * @param boolean $skipEmpty Whether to skip empty strings between delimiters. Default is false.
	 * @return array
	 */
	public static function explode(string $string, string $delimiter = ',', mixed $trim = true, bool $skipEmpty = false) : array {
		$result = explode($delimiter, $string);
		if ($trim) {
			if ($trim === true) $trim = 'trim';
			elseif (!is_callable($trim)) {
				$trim = function($v) use ($trim) {
					return trim($v, $trim);
				};
			}
			$result = array_map($trim, $result);
		}
		if ($skipEmpty) {
			// Wrapped with array_values to make array keys sequential after empty values removing
			$result = array_values(array_filter($result, function ($value) {
				return $value !== '';
			}));
		}
		return $result;
	}

    /**
     * Extracts a portion of a string.
     *
     * @param string $string
     * @param int $offset
     * @param int|null $length
     * @return string
     */
	public static function extract(string $string, int $offset, ?int $length = null) : string {
		if ($length) return mb_substr($string, $offset, $length);
		return mb_substr($string, $offset);
	}

	/**
	 * Grab the index of the first matched character. Returns false when the needle is not found.
	 * If `$strict` is passed as `false` then the evaluation will made case insensitive.
	 *
	 * @param string $string
	 * @param string $needle
	 * @param int $offset
	 * @param bool $strict
	 * @return bool|int
	 */
	public static function indexOf(string $string, string $needle, int $offset = 0, bool $strict = true) {
		if($strict) return mb_strpos($string, $needle, $offset);
		else return mb_stripos($string, $needle, $offset);
	}

	/**
	 * Checks to see if the string starts with a specific value.
	 *
	 * @param string $string
	 * @param string $needle
	 * @param bool $strict
	 * @return bool
	 */
	public static function startsWith(string $string, string $needle, bool $strict = true) : bool {
		$start = static::extract($string, 0, mb_strlen($needle));
		if ($strict) return ($start === $needle);
		return (mb_strtolower($start) === mb_strtolower($needle));
	}

	/**
	 * Truncates a string to the number of characters specified.
	 *
	 * @param string $string The string to truncate.
	 * @param int $length How many characters from original string to include into truncated string.
	 * @param string $suffix String to append to the end of truncated string.
	 * @param string $encoding The charset to use, defaults to charset currently used by application.
	 *
	 * @return string the truncated string.
	 */
	public static function truncate(string $string, int $length, string $suffix = '...', string $encoding = 'UTF-8') : string {
		if (mb_strlen($string, $encoding) > $length) return rtrim(mb_substr($string, 0, $length, $encoding)) . $suffix;
		return $string;
	}

	/**
	 * Truncates a string to the number of words specified.
	 *
	 * @param string $string The string to truncate.
	 * @param int $count How many words from original string to include into truncated string.
	 * @param string $suffix String to append to the end of truncated string.
	 *
	 * @return string the truncated string.
	 */
	public static function truncateWords(string $string, int $count, string $suffix = '...') : string {
		$words = preg_split('/(\s+)/u', trim($string), -1, PREG_SPLIT_DELIM_CAPTURE);
		if (count($words) / 2 > $count) {
			return implode('', array_slice($words, 0, ($count * 2) - 1)) . $suffix;
		}
		return $string;
	}

	/**
	 * This method provides a unicode-safe implementation of built-in PHP function `ucfirst()`.
	 *
	 * @param string $string the string to be proceeded
	 * @param string $encoding Optional, defaults to "UTF-8"
	 * @return string
	 * @see http://php.net/manual/en/function.ucfirst.php
	 */
	public static function mbUcFirst(string $string, string $encoding = 'UTF-8') : string {
		$firstChar = mb_substr($string, 0, 1, $encoding);
		$rest = mb_substr($string, 1, null, $encoding);
		return mb_strtoupper($firstChar, $encoding) . $rest;
	}

	/**
	 * This method provides a unicode-safe implementation of built-in PHP function `ucwords()`.
	 *
	 * @param string $string the string to be proceeded
	 * @param string $encoding Optional, defaults to "UTF-8"
	 * @see http://php.net/manual/en/function.ucwords.php
	 * @return string
	 */
	public static function mbUcWords(string $string, string $encoding = 'UTF-8') : string {
		$words = preg_split("/\s/u", $string, -1, PREG_SPLIT_NO_EMPTY);
		$titelized = array_map(function($word) use ($encoding) {
			return static::mbUcFirst($word, $encoding);
		}, $words);

		return implode(' ', $titelized);
	}
}
