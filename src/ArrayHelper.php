<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote;

use xibalba\ocelote\traits\ArrayChecker;

/**
 * This class provide static methods for array manipulation.
 * Some are improved wrappers or syntactic sugar of php common used
 * array functions. Use those methods for a better OOP cohesion on your projects.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\ocelote
 */
class ArrayHelper {
	use ArrayChecker;

	/**
	 * Determines whether the given array contains a given value.
	 *
	 * @param array $array
	 * @param $value
	 */
	public static function hasValue(array $array, $value, bool $strict = false) : bool {
		return in_array($value, $array, $strict);
	}

	 /**
	 * Returns the values of a specified column in an array.
	 * The input array should be multidimensional or an array of objects.
	 *
	 * For example,
	 *
	 * ~~~
	 * $array = [
	 *     ['id' => '123', 'data' => 'abc'],
	 *     ['id' => '345', 'data' => 'def'],
	 * ];
	 * $result = ArrayHelper::getColumn($array, 'id');
	 * // the result is: ['123', '345']
	 *
	 * // using anonymous function
	 * $result = ArrayHelper::getColumn($array, function ($element) {
	 *     return $element['id'];
	 * });
	 * ~~~
	 *
	 * @param array $array
	 * @param string|\Closure $name
	 * @param bool $keepKeys whether to maintain the array keys. If false, the resulting array
	 * will be re-indexed with integers.
	 *
	 * @return array the list of column values
	 */
	public static function getColumn(array $array, $name, bool $keepKeys = true) : array {
		$result = [];
		if($keepKeys) {
			foreach($array as $k => $element) {
				$result[$k] = static::getValue($element, $name);
			}
		}
        else {
			foreach($array as $element) {
				$result[] = static::getValue($element, $name);
			}
		}

		return $result;
	}

	/**
	 * Return all the keys or a subset of the keys of an array.
	 * OOP sugar for `array_keys` function.
	 *
	 * @param array $array
	 * @param mixed $searchValue
	 * @param bool $strict
	 * @return array
	 */
	public static function getKeys(array $array, mixed $searchValue = null, bool $strict = false) : array {
		return array_keys($array, $searchValue, $strict);
	}

	/**
	 * Search for the index of `$needle` from an indexed array, and if found it is returned. Otherwise false is returned.
	 * If passed array is associative, then false is also returned.
	 *
	 * @param array $array Indexed array
	 * @param mixed $needle Value to search
	 * @param bool $strict if set to true will search for identical elements
	 * @return mixed founded index or false
	 */
	public static function getIndex(array $array, mixed $needle, bool $strict = false) {
		if(static::isIndexed($array)) return array_search($needle, $array, $strict);
		return false;
	}

	/**
	 * Retrieves the value of an array element or object property with the given key or property name.
	 * If the key does not exist in the array or object, the default value will be returned instead.
	 *
	 * The key may be specified in a dot format to retrieve the value of a sub-array or the property
	 * of an embedded object. In particular, if the key is `x.y.z`, then the returned value would
	 * be `$array['x']['y']['z']` or `$array->x->y->z` (if `$array` is an object). If `$array['x']`
	 * or `$array->x` is neither an array nor an object, the default value will be returned.
	 * Note that if the array already has an element `x.y.z`, then its value will be returned
	 * instead of going through the sub-arrays. So it is better to be done specifying an array of key names
	 * like `['x', 'y', 'z']`.
	 *
	 * Below are some usage examples,
	 *
	 * ~~~
	 * // working with array
	 * $username = \yii\helpers\ArrayHelper::getValue($_POST, 'username');
	 * // working with object
	 * $username = \yii\helpers\ArrayHelper::getValue($user, 'username');
	 * // working with anonymous function
	 * $fullName = \yii\helpers\ArrayHelper::getValue($user, function ($user, $defaultValue) {
	 *     return $user->firstName . ' ' . $user->lastName;
	 * });
	 * // using dot format to retrieve the property of embedded object
	 * $street = \yii\helpers\ArrayHelper::getValue($users, 'address.street');
	 * // using an array of keys to retrieve the value
	 * $value = \yii\helpers\ArrayHelper::getValue($versions, ['1.0', 'date']);
	 * ~~~
	 *
	 * @param array|object $array array or object to extract value from
	 * @param string|\Closure|array $key key name of the array element, an array of keys or property name of the object,
	 * or an anonymous function returning the value. The anonymous function signature should be:
	 * `function($array, $defaultValue)`.
	 * The possibility to pass an array of keys is available since version 2.0.4.
	 * @param mixed $default the default value to be returned if the specified array key does not exist. Not used when
	 * getting value from an object.
	 * @return mixed the value of the element if found, default value otherwise
	 * @throws InvalidParamException if $array is neither an array nor an object.
	 */
	public static function getValue(array|object $array, mixed $key, mixed $default = null) : mixed {
		if($key instanceof \Closure)  return $key($array, $default);

		if(is_array($key)) {
			$lastKey = array_pop($key);
			foreach ($key as $keyPart) $array = static::getValue($array, $keyPart);
			$key = $lastKey;
		}

		if(is_array($array) && array_key_exists($key, $array)) return $array[$key];

		if(($pos = strrpos($key, '.')) !== false) {
			$array = static::getValue($array, substr($key, 0, $pos), $default);
			$key = substr($key, $pos + 1);
		}

		if (is_object($array)) return $array->$key;
		elseif (is_array($array)) return array_key_exists($key, $array) ? $array[$key] : $default;
		else return $default;
	}

	/**
	 * Retrives a new array from the given array containing only the providen keys.
	 * If a null value is found and $includeNulls flag is false, then that key-value pair
	 * is ignored. Otherwise the key-value pair is included with the null value.
	 *
	 * @param array $array array to extract value from.
	 * @param array $keys array of keys to retrive.
	 * @param bool $includeNulls set if null values must be included on the result array.
	 *
	 * @return $array the resutl array
	 */
	public static function getValues(array $array, array $keys, bool $includeNulls = false) : array {
		$r = [];

		if (Checker::isIndexed($keys)) {
			foreach($keys as $key) {
				if($includeNulls) $r[$key] = static::getValue($array, $key, null);
				else {
					$tmp = static::getValue($array, $key, null);
					if(!is_null($tmp)) $r[$key] = $tmp;
				}
			}

			return $r;
		}

		return [];
	}

	/**
	 * Determines whether the given array contains the specified key.
	 * This method enhances the `array_key_exists()` function by supporting case-insensitive
	 * key comparison.
	 *
	 * @param array $array the array with keys to check
     * @param string $key the key to check
	 * @param boolean | true $caseSensitive whether the key comparison should be case-sensitive
	 *
	 * @return boolean whether the array contains the specified key
	 */
	public static function hasKey(array $array, string $key, bool $caseSensitive = true) : bool {
		if ($caseSensitive) return array_key_exists($key, $array);
		else {
			foreach (array_keys($array) as $k) if (strcasecmp($key, $k) === 0) return true;
			return false;
		}
	}

	/**
	 * Checks if the given array contains the specified keys on array keys.
	 *
	 * @param array $array the array with keys to check.
	 * @param array $keys as array with keys for check.
	 * @param boolean | true $caseSensitive whether the key comparison should be case-sensitive
	 *
	 * @return bool whether the array contains the specified key
	 */
	public static function hasKeys(array $array, array $keys, bool $caseSensitive = true) : bool {
		if (static::isIndexed($keys)) {
			foreach($keys as $key) {
				if(!static::hasKey($array, $key, $caseSensitive)) return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Alias to ::hasKey()
	 * CAUTION: This method has a diferent parameter order.
	 *
	 * @deprecated
	 */
	public static function keyExists($key, $array, $caseSensitive = true) {
		return static::hasKey($array, $key, $caseSensitive);
	}

	/**
	 * Alias to ::hasKeys()
	 * CAUTION: This method has a diferent parameter order.
	 *
	 * @deprecated
	 */
	public static function keysExist($keys, $array, $caseSensitive = true) {
		return static::hasKeys($array, $keys, $caseSensitive);
	}

	 /**
	 * Builds a map (key-value pairs) from a multidimensional array or an array of objects.
	 * The `$from` and `$to` parameters specify the key names or property names to set up the map.
	 * Optionally, one can further group the map according to a grouping field `$group`.
	 *
	 * For example,
	 *
	 * ~~~
	 * $array = [
	 *     ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
	 * 	   ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
	 *     ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
	 * ];
	 *
	 * $result = ArrayHelper::map($array, 'id', 'name');
	 * // the result is:
	 * // [
	 *     // '123' => 'aaa',
	 *     // '124' => 'bbb',
	 *     // '345' => 'ccc',
	 * // ]
	 *
	 * $result = ArrayHelper::map($array, 'id', 'name', 'class');
	 * // the result is:
	 * // [
	 *     // 'x' => [
	 *         // '123' => 'aaa',
	 *         // '124' => 'bbb',
	 *     // ],
	 *     // 'y' => [
	 *         // '345' => 'ccc',
	 *         // ],
	 *     // ]
	 * ~~~
	 *
	 * @param array $array
	 * @param string|\Closure $from
	 * @param string|\Closure $to
	 * @param string|\Closure $group
	 *
	 * @return array
	 */
	public static function map(array $array, mixed $from, mixed $to, mixed $group = null) : array {
		$result = [];
		foreach ($array as $element) {
			$key = static::getValue($element, $from);
			$value = static::getValue($element, $to);
			if ($group !== null) $result[static::getValue($element, $group)][$key] = $value;
			else $result[$key] = $value;
		}

		return $result;
	}

	/**
	 * Merges two or more arrays into one recursively.
	 * If each array has an element with the same string key value, the latter
	 * will overwrite the former (different from array_merge_recursive).
	 * Recursive merging will be conducted if both arrays have an element of array
	 * type and are having the same key.
	 * For integer-keyed elements, the elements from the latter array will
	 * be appended to the former array.
	 *
	 * @param array $a array to be merged to
	 * @param array $b array to be merged from. You can specify additional
	 * arrays via third argument, fourth argument etc.
	 * @return array the merged array (the original arrays are not changed.)
	 */
	public static function merge(array $a, array $b) : array {
		$args = func_get_args();
		$res = array_shift($args);

		while (!empty($args)) {
			$next = array_shift($args);
			foreach ($next as $k => $v) {
				if (is_integer($k)) {
					if (isset($res[$k])) $res[] = $v;
					else $res[$k] = $v;
				} elseif (is_array($v) && isset($res[$k]) && is_array($res[$k])) {
					$res[$k] = self::merge($res[$k], $v);
				}
				else $res[$k] = $v;
			}
		}

		return $res;
	}

	/**
	 * Removes an item from an array and returns the value. If the key does not exist in the array, the default value
	 * will be returned instead.
	 *
	 * If the given array is indexed, and `$reindex` is true, then the given array will be reindexed.
	 *
	 * Caution: this method mutates the passed array!
	 *
	 * Usage examples,
	 *
	 * ~~~
	 * // $array = ['type' => 'A', 'options' => [1, 2]];
	 * // working with array
	 * $type = \xibalba\ocetole\helpers\ArrayHelper::remove($array, 'type');
	 * // $array content
	 * // $array = ['options' => [1, 2]];
	 *
	 * // $array = ['water', 'coffee', 'milk', 'beer'];
	 * $val = \xibalba\ocetole\helpers\ArrayHelper::remove($array, 2);
	 * // $array content with indexes
	 * // $array = [0 => 'water', 1 => 'coffee', 2 => 'beer'];
	 *
	 * // $array = ['water', 'coffee', 'milk', 'beer'];
	 * $val = \xibalba\ocetole\helpers\ArrayHelper::remove($array, 2, false);
	 * // $array content with indexes
	 * // $array = [0 => 'water', 1 => 'coffee', 3 => 'beer'];
	 * ~~~
	 *
	 * @param array $array the array to extract value from
	 * @param integer|string $key key name or index of the array element
	 * @param boolean $reindex true for reindex indexed based array
	 * @param mixed $default the default value to be returned if the specified key does not exist
	 * @return mixed|null the value of the element if found, default value otherwise
	 */
	public static function remove(array &$array, mixed $key, bool $reindex = true, mixed $default = null)  : mixed{
		if(isset($array[$key]) || array_key_exists($key, $array)) {
			$value = $array[$key];
			unset($array[$key]);
			if(static::isIndexed($array) && $reindex) $array = array_values($array);

			return $value;
		}

		return $default;
	}

	/**
	 * Search for a value in an indexed array and, if foud it, remove it. If the value does not exist, then a default
	 * value will be returned.
	 * If the passed array is not indexed, then a defoult value will be returned.
	 * If `$reindex` is true, then the given array will be reindexed.
	 *
	 * Caution: this method mutates the passed array!
	 *
	 * Usage examples,
	 *
	 * ~~~
	 * // $array = ['type' => 'A', 'options' => [1, 2]];
	 * // try with as associative array return defaul
	 * $type = \xibalba\ocetole\helpers\ArrayHelper::searchAndRemove($array, 'type');
	 * // $type content is false

	 * // $array = ['water', 'coffee', 'milk', 'beer'];
	 * $val = \xibalba\ocetole\helpers\ArrayHelper::searchAndRemove($array, 'milk');
	 * // $array content with indexes
	 * // $array = [0 => 'water', 1 => 'coffe', 2 => 'beer'];
	 *
	 * // $array = ['water', 'coffee', 'milk', 'beer'];
	 * $val = \xibalba\ocetole\helpers\ArrayHelper::searchAndRemove($array, 'water', true, false);
	 * // $array content with indexes
	 * // $array = [1 => 'coffe', 2 => 'milk', 3 => 'beer'];
	 * ~~~
	 *
	 * @param array $array the array to extract value from
	 * @param mixed $needle value for search and remove
	 * @param boolean $reindex true for reindex indexed based array
	 * @param mixed $default the default value to be returned if the specified key does not exist
	 * @return mixed|null the value of the element if found, default value otherwise
	 */
	public static function searchAndRemove(array &$array, mixed $needle, bool $strict = false, bool $reindex = true, mixed $default = false) : mixed {
		$idx = static::getIndex($array, $needle, $strict);
		if($idx === false) return $default;
		else return static::remove($array, $idx, $reindex, $default);
	}
}
