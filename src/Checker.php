<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote;

use xibalba\ocelote\interfaces\Bag as IBag;
use xibalba\ocelote\traits\ArrayChecker;

/**
 * This class provide static methods for check XML, JSON and Serialized datatype.
 * Also provide static methods to wraps is_{type}() function for Syntactic sugar purpose.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package alpaca\ocelote
 */
class Checker {
	use ArrayChecker;

	/**
	 * Returns a string for the detected type.
	 *
	 * @param mixed $data
	 * @return string
	 */
	public static function is(mixed $data) : string {
		if (static::isArray($data)) return 'array';
		else if (static::isObject($data)) return 'object';
		else if (static::isJson($data)) return 'json';
		else if (static::isSerialized($data)) return 'serialized';
		else if (static::isXml($data)) return 'xml';
		else if (static::isBag($data)) return 'ocelote\Bag';

		// Attempt other types
		return strtolower(gettype($data));
	}

	/**
	 * check to see if data passed is a ocelote Bag instance.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isBag(mixed $data) : bool {
		return ($data instanceof IBag);
	}

	/**
	 * Check to see if data passed is a boolean type.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isBoolean(mixed $data) : bool {
		return is_bool($data);
	}

	/**
	 * Check to see if data passed is empty
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isEmpty(mixed $data) : bool {
		return empty($data);
	}

	/**
	 * Check to see if data passed is a float type.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isFloat(mixed $data) : bool {
		return is_float($data);
	}

	/**
	 * Check to see if data passed is an interger type.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isInteger(mixed $data) : bool {
		return is_int($data);
	}

	/**
	 * Check to see if data passed is a JSON object.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isJson(mixed $data) : bool {
		if (!is_string($data) || empty($data)) return false;

		$json = @json_decode($data, true);
		return (json_last_error() === JSON_ERROR_NONE && $json !== null);
	}

	/**
	 * Check to see if data passed is a numeric type.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isNumeric(mixed $data) : bool {
		return is_numeric($data);
	}

	/**
	 * Check to see if data passed is an object.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isObject(mixed $data) : bool {
		return is_object($data);
	}

	/**
	 * Check to see if data passed has been serialized.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isSerialized(mixed $data) : bool {
		if (!is_string($data) || empty($data)) return false;
		return (@unserialize($data) !== false);
	}

	/**
	 * Check to see if data passed is a scalar type.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isScalar(mixed $data) : bool {
		return is_scalar($data);
	}

	/**
	 * Check to see if data passed is a string
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isString(mixed $data) : bool {
		return is_string($data);
	}

	/**
	 * Check to see if data passed is an XML document.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isXml(mixed $data) : bool {
		// Do manually checks on the string since HHVM blows up
		if (!is_string($data) || substr($data, 0, 5) !== '<?xml') return false;
		return (@simplexml_load_string($data) instanceof \SimpleXMLElement);
	}
}
