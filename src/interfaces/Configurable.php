<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote\interfaces;

/**
 * Include this interface for ensure that an class can support configuration methods.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\ocelote\interfaces
 */
interface Configurable {
	/**
	 * Set an associative array as configuration data.
	 */
	public function setConfig(array $config);

	/**
	 * Return the configuration value identified by key.
	 * If the key is no set, a default value can be returned.
	 *
	 * @param string $key Key configuration to get.
	 * @param mixed $default default value to return if key is no set.
	 *
	 * @return mixed value.
	 */
	public function getConfig(string $key, mixed $default = null);

	/**
	 * Return all the configuration data
	 *
	 * @return array The configuration data array.
	 */
	public function getAllConfig() : array;

	/**
	 * Return a bool value whenever the configuration is set or not.
	 */
	public function hasConfig() : bool;
}
