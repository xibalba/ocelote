<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote\interfaces;

/**
 * A bag can be used for managing sets of specialized data.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\ocelote\interfaces
 */
interface Bag {
	/**
	 * Add multiple parameters that will overwrite any previously
	 * defined parameters.
	 *
	 * @param array $params
	 * @return $this
	 */
	public function add(array $params) : static;

	/**
	 * Return all parameters and their values within the bag.
	 *
	 * @return array
	 */
	public function all() : array;

	/**
	 * Remove all values within the bag.
	 *
	 * @return $this
	 */
	public function flush() : static;

	/**
	 * Return a value defined by key, or by dot notated path.
	 * If no key is found, return null, or if there is no value,
	 * return the default value.
	 *
	 * @param string $key
	 * @param mixed $default
	 * @return string
	 */
	public function get(string $key, mixed $default = null) : mixed;

	/**
	 * Set a value defined by key. Can pass in a dot notated path
	 * to insert into a nested structure.
	 *
	 * @param string $key
	 * @param string $value
	 * @return $this
	 */
	public function set(string $key, mixed $value) : static;

	/**
	 * Check if a key exists within the bag.
	 * Can use a dot notated path as the key.
	 *
	 * @param string $key
	 * @return bool
	 */
	public function has(string $key) : bool;

	/**
	 * Remove a value defined by key, or dot notated path.
	 *
	 * @param string $key
	 * @return $this
	 */
	public function remove(string $key) : static;
}
