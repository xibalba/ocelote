<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote;

use xibalba\ocelote\interfaces\Bag as IBag;
use xibalba\ocelote\traits\Mutable;

/**
 * This class is an implementation of Mutable for satisfy Bag interface.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\ocelote
 */
class Bag implements IBag { use Mutable; }
