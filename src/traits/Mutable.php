<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote\traits;

use \ArrayIterator;

/**
 * The Mutable trait allows for a set of data to be read and written to.
 * It also provides methods for array access, iteration and counting.
 * This trait is useful when an object should represent a set of data or an entity.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package xibalba\ocelote\traits
 */
trait Mutable {
	/**
	 * Mapped data.
	 *
	 * @type array
	 */
	protected array $_data = [];

	/**
	 * Magic method for get().
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function __get(string $key) {
		return $this->get($key);
	}

	/**
	 * Magic method for set().
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function __set(string $key, mixed $value) {
		$this->set($key, $value);
	}

	/**
	 * Magic method for has().
	 *
	 * @param string $key
	 * @return bool
	 */
	public function __isset(string $key) {
		return $this->has($key);
	}

	/**
	 * Magic method for remove().
	 *
	 * @param string $key
	 */
	public function __unset(string $key) {
		$this->remove($key);
	}

	/**
	 * Add multiple parameters.
	 *
	 * @param array $params
	 * @return $this
	 */
	public function add(array $params) : static {
		foreach ($params as $key => $value) $this->set($key, $value);
		return $this;
	}

	/**
	 * Return all parameters.
	 *
	 * @return array
	 */
	public function all() : array {
		return $this->_data;
	}

	/**
	 * Reset all data.
	 *
	 * @return $this
	 */
	public function flush() : static {
		$this->_data = [];
		return $this;
	}

	/**
	 * Return a parameter by key.
	 *
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get(string $key, $default = null) : mixed {
		$value = $this->_data[$key] ?? null;
		if ($value === null) return $default;
		return $value;
	}

	/**
	 * Return an iterator.
	 *
	 * @return \ArrayIterator
	 */
	public function getIterator() : ArrayIterator {
		return new ArrayIterator($this->_data);
	}

	/**
	 * Check if a parameter exists.
	 *
	 * @param string $key
	 * @return bool
	 */
	public function has(string $key) : bool {
		return isset($this->_data[$key]);
	}

	/**
	 * Return all the parameter keys.
	 *
	 * @return array
	 */
	public function keys() : array {
		return array_keys($this->_data);
	}

	/**
	 * Remove a parameter by key.
	 *
	 * @param string $key
	 * @return $this
	 */
	public function remove(string $key) : static {
		unset($this->_data[$key]);
		return $this;
	}

	/**
	 * Set a parameter value by key.
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return $this
	 */
	public function set(string $key, mixed $value = null) : static {
		$this->_data[$key] = $value;
		return $this;
	}

	/**
	 * Return the data as an array.
	 *
	 * @return array
	 */
	public function toArray() : array {
		return $this->_data;
	}

	/**
	 * Alias method for get().
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function offsetGet(string $key) : mixed {
		return $this->get($key);
	}

	/**
	 * Alias method for set().
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function offsetSet(string $key, mixed $value) : static {
		return $this->set($key, $value);
	}

	/**
	 * Alias method for has().
	 *
	 * @param string $key
	 * @return bool
	 */
	public function offsetExists($key) : bool {
		return $this->has($key);
	}

	/**
	 * Alias method for remove().
	 *
	 * @param string $key
	 */
	public function offsetUnset(string $key) : static {
		return $this->remove($key);
	}

	/**
	 * Return the count of the array.
	 *
	 * @return int
	 */
	public function count() : int {
		return count($this->_data);
	}
}
