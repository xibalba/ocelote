<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote\traits;

trait ArrayChecker {
	/**
	 * Check to see if data passed is an array.
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public static function isArray($data) : bool {
		return is_array($data);
	}

	/**
	 * Returns a value indicating whether the given array is an associative array.
	 *
	 * An array is associative if at least one of its keys is a string.
	 * If `$allStrings` is true, then an array will be treated as associative only
	 * if all of its keys are strings.
	 *
	 * Note that an empty array will NOT be considered associative.
	 *
	 * @param array $array the array being checked
	 * @param bool $allStrings whether the array keys must be all strings in order for
	 * the array to be treated as associative.
	 *
	 * @return boolean whether the array is associative
	 */
	public static function isAssociative(array $array, bool $allStrings = false) : bool {
		if(empty($array)) return false;

		if($allStrings) {
			foreach ($array as $key => $value) if (!is_string($key)) return false;
			return true;
		}
		else {
			foreach ($array as $key => $value) if (is_string($key)) return true;
			return false;
		}
	}

	/**
	 * Returns a value indicating whether the given array is an indexed array.
	 *
	 * An array is indexed if all its keys are integers. If `$consecutive` is true,
	 * then the array keys must be a consecutive sequence starting from 0.
	 *
	 * Note that an empty array will be considered indexed.
	 *
	 * @param array $array the array being checked
	 * @param bool $consecutive whether the array keys must be a consecutive sequence
	 * in order for the array to be treated as indexed.
	 *
	 * @return boolean whether the array is associative
	 */
	public static function isIndexed(array $array, bool $consecutive = false) : bool {
		if (empty($array)) return true;

		if ($consecutive) return array_keys($array) === range(0, count($array) - 1);
		else {
			foreach($array as $key => $value) if(!is_integer($key)) return false;
			return true;
		}
	}

	/**
	 * Returns a value indicating whether *all* values on given array are empty.
	 * An array of keys (or indexes) to check only those values on given array can be provided.
	 * If any of values to check on given array is not empty then a `false` value vill be returned.
	 *
	 * @param array $array the array to check.
	 * @param array $keys optional array of keys (or indexes) to restrct checking on given array.
	 * @param boolean | true $caseSensitive whether the key comparison should be case-sensitive.
	 *
	 * @return boolean whether the checked values on given array are empty.
	 */
	public static function hasEmptyValues(array $array, array $keys = [], bool $caseSensitive = true) : bool {
		if(empty($keys)) {
			// Check the whole array
			foreach ($array as $el) {
				if(!empty($el)) return false;
			}

			return true;
		}
		else if (static::isIndexed($keys)) {
			if( !static::hasKeys($array, $keys, $caseSensitive) ) throw new \InvalidArgumentException("Provided key does not exists on array.");

			foreach($keys as $key) {
				if( !empty($array[$key]) ) return false;
			}

			return true;
		}

		throw new \InvalidArgumentException("Malformed keys array were provided.");
	}
}
