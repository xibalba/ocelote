<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote\traits;

use xibalba\ocelote\Checker;

/**
 * This trait provide a property and methods for handle configuration items
 * on clases. A configuration array ever must be associative.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 * @package alpaca\ocelote\traits
 */
trait ConfigAware {
	protected array $_config = [];

	/**
	 * Set an associative array to
	 */
	public function setConfig(array $config) {
		if(!Checker::isAssociative($config, true)) throw new \Exception('Configuration array malformed.');
		$this->_config = $config;
	}

	/**
	 * Return the configuration value identified by key.
	 * If the key is no set, a default value can be returned.
	 *
	 * @param string $key Key configuration to get.
	 * @param mixed $default default value to return if key is no set.
	 *
	 * @return mixed value.
	 */
	public function getConfig(string $key, mixed $default = null) : mixed {
		if(isset($this->_config[$key])) return $this->_config[$key];
		return $default;
	}

	public function getAllConfig() : array {
		if(!empty($this->_config)) return $this->_config;
		throw new \Exception("Configuration is empty");
	}

	public function hasConfig() : bool {
		return !empty($this->_config);
	}
}
