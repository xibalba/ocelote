<?php
/**
 * @copyright 2014 - 2024 Xibalba Lab.
 * @license   http://opensource.org/licenses/bsd-license.php
 * @link      https://gitlab.com/xibalba/ocelote
 */

namespace xibalba\ocelote\traits;

/**
 * Provide a Singleton pattern trait for classes.
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait SingletonAware {
	/**
	 * The singleton instance
	 * @type object
	 */
	protected static $_instance = null;

	/**
	 * Construct a Singleton object.
	 * Do not allow explicit instance
	 */
	protected function __construct() {}

	// Do not allow to clone
	protected function __clone() {}

	/**
	 * Return singleton application object.
	 *
	 * @return object singleton application object.
	 */
	public static function getInstance() {
		if(static::$_instance === null) static::$_instance = new static();
		return static::$_instance;
	}
}
