<?php

namespace michiq;

use PHPUnit\Framework\TestCase;
use xibalba\ocelote\Bag;

/**
 * Since Bag is an implementation of Bag interface and Mutable trait,
 * this class is realy a test for Mutable trait.
 */
class BagTest extends TestCase {
	private $__bag;
	
	private $__bagData = [
		'xibalba' => ['ocelote', 'tuza'],
		'kinich' => ['yax', 'kuk', 'mo']
	];
	
	public function setUp() {
		$this->__bag = new Bag();
	}
	
	public function testBag() {
		$this->__bag->add($this->__bagData);
		
		$this->assertEquals($this->__bagData, $this->__bag->all());
		$this->assertEquals(['ocelote', 'tuza'], $this->__bag->get('xibalba'));
		$this->assertEquals('balam', $this->__bag->get('balam', 'balam'));
		$this->assertEquals(['xibalba', 'kinich'], $this->__bag->keys());
		$this->assertEquals(2, $this->__bag->count());
		$this->assertTrue($this->__bag->has('kinich'));
		
		$this->__bag->remove('kinich');
		
		$this->assertFalse($this->__bag->has('kinich'));
		
		$this->__bag->flush();
		
		$this->assertEquals([], $this->__bag->all());
	}
}