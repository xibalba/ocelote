<?php

namespace michiq;

use PHPUnit\Framework\TestCase;
use xibalba\ocelote\Inflector;

class InflectorTest extends TestCase {
	public function testCamelize(){
		$this->assertEquals("MeMySelfAndI", Inflector::camelize('me my_self-andI'));
        $this->assertEquals("QweQweEwq", Inflector::camelize('qwe qwe^ewq'));
	}
	
	public function testClassify(){
		$this->assertEquals("HerdTable", Inflector::classify('herd_tables'));
	}
	
	public function testFilenamize(){
		$this->assertEquals('Llamas.php', Inflector::filenamize('llamas'));
		$this->assertEquals('Llamas.hh', Inflector::filenamize('llamas', 'hh'));
		$this->assertEquals('llamas.hh', Inflector::filenamize('llamas', 'hh', false));
	}
	
	public function testHyphenate(){
		$this->assertEquals('herd-llama', Inflector::hyphenate('herd llama'));
	}
	
	public function testHumanize(){
		$this->assertEquals("Me my self and i", Inflector::humanize('me_my_self_and_i'));
        $this->assertEquals("Me My Self And I", Inflector::humanize('me_my_self_and_i', true));
	}
	
	public function testOrdinalize(){
		$this->assertEquals('21st', Inflector::ordinalize('21'));
        $this->assertEquals('22nd', Inflector::ordinalize('22'));
        $this->assertEquals('23rd', Inflector::ordinalize('23'));
        $this->assertEquals('24th', Inflector::ordinalize('24'));
        $this->assertEquals('25th', Inflector::ordinalize('25'));
        $this->assertEquals('111th', Inflector::ordinalize('111'));
        $this->assertEquals('113th', Inflector::ordinalize('113'));
	}
	
	public function testPluralize(){
		$testData = [
            'move' => 'moves',
            'foot' => 'feet',
            'child' => 'children',
            'human' => 'humans',
            'man' => 'men',
            'staff' => 'staff',
            'tooth' => 'teeth',
            'person' => 'people',
            'mouse' => 'mice',
            'touch' => 'touches',
            'hash' => 'hashes',
            'shelf' => 'shelves',
            'potato' => 'potatoes',
            'bus' => 'buses',
            'test' => 'tests',
            'car' => 'cars',
        ];

        foreach ($testData as $testIn => $testOut) {
            $this->assertEquals($testOut, Inflector::pluralize($testIn));
            $this->assertEquals(ucfirst($testOut), ucfirst(Inflector::pluralize($testIn)));
        }
	}
	
	public function testSingularize(){
		$testData = [
            'moves' => 'move',
            'feet' => 'foot',
            'children' => 'child',
            'humans' => 'human',
            'men' => 'man',
            'staff' => 'staff',
            'teeth' => 'tooth',
            'people' => 'person',
            'mice' => 'mouse',
            'touches' => 'touch',
            'hashes' => 'hash',
            'shelves' => 'shelf',
            'potatoes' => 'potato',
            'buses' => 'bus',
            'tests' => 'test',
            'cars' => 'car',
        ];
        foreach ($testData as $testIn => $testOut) {
            $this->assertEquals($testOut, Inflector::singularize($testIn));
            $this->assertEquals(ucfirst($testOut), ucfirst(Inflector::singularize($testIn)));
        }
	}
	
	public function testSlugCommons(){
		$data = [
            '' => '',
            'hello world 123' => 'hello-world-123',
            'remove.!?[]{}…symbols' => 'removesymbols',
            'minus-sign' => 'minus-sign',
            'mdash—sign' => 'mdash-sign',
            'ndash–sign' => 'ndash-sign',
            'áàâéèêíìîóòôúùûã' => 'aaaeeeiiiooouuua',
            'älä lyö ääliö ööliä läikkyy' => 'ala-lyo-aalio-oolia-laikkyy',
        ];

        foreach ($data as $source => $expected) {
            $this->assertEquals($expected, Inflector::slug($source));
        }
	}
	
	public function testSlugIntl(){
		if (!extension_loaded('intl')) {
            $this->markTestSkipped('intl extension is required.');
        }

        // Some test strings are from https://github.com/bergie/midgardmvc_helper_urlize. Thank you, Henri Bergius!
        $data = [
            // Korean
            '해동검도' => 'haedong-geomdo',

            // Hiragana
            'ひらがな' => 'hiragana',

            // Georgian
            'საქართველო' => 'sakartvelo',

            // Arabic
            'العربي' => 'alrby',
            'عرب' => 'rb',

            // Hebrew
            'עִבְרִית' => 'iberiyt',

            // Turkish
            'Sanırım hepimiz aynı şeyi düşünüyoruz.' => 'sanrm-hepimiz-ayn-seyi-dusunuyoruz',

            // Russian
            'недвижимость' => 'nedvizimost',
            'Контакты' => 'kontakty',
        ];

        foreach ($data as $source => $expected) {
            $this->assertEquals($expected, Inflector::slug($source));
        }
	}
	
	public function testTableize(){
		$this->assertEquals("herd_tables", Inflector::tableize('HerdTable'));
	}
	
	public function testTitleize(){
		$this->assertEquals("Me my self and i", Inflector::titleize('MeMySelfAndI'));
        $this->assertEquals("Me My Self And I", Inflector::titleize('MeMySelfAndI', true));
	}
	
	public function testUnderscore(){
		$this->assertEquals("me_my_self_and_i", Inflector::underscore('MeMySelfAndI'));
	}
	
	public function testViriablize(){
		$this->assertEquals("_23_llamas", Inflector::variablize('23_llamas'));
	}
}