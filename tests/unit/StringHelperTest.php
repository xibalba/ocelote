<?php

namespace michiq;


use PHPUnit\Framework\TestCase;
use xibalba\ocelote\StringHelper;

class StringHelperTest extends TestCase {
	protected $_string = 'Ocelote Utility Library';
	
	public function testBasename() {
		$this->assertEquals('', StringHelper::basename(''));

		$this->assertEquals('file', StringHelper::basename('file'));
		$this->assertEquals('file.test', StringHelper::basename('file.test', '.test2'));
		$this->assertEquals('file', StringHelper::basename('file.test', '.test'));

		$this->assertEquals('file', StringHelper::basename('/file'));
		$this->assertEquals('file.test', StringHelper::basename('/file.test', '.test2'));
		$this->assertEquals('file', StringHelper::basename('/file.test', '.test'));
	
		$this->assertEquals('file', StringHelper::basename('/path/to/file'));
		$this->assertEquals('file.test', StringHelper::basename('/path/to/file.test', '.test2'));
		$this->assertEquals('file', StringHelper::basename('/path/to/file.test', '.test'));
	
		$this->assertEquals('file', StringHelper::basename('\file'));
		$this->assertEquals('file.test', StringHelper::basename('\file.test', '.test2'));
		$this->assertEquals('file', StringHelper::basename('\file.test', '.test'));

		$this->assertEquals('file', StringHelper::basename('C:\file'));
		$this->assertEquals('file.test', StringHelper::basename('C:\file.test', '.test2'));
		$this->assertEquals('file', StringHelper::basename('C:\file.test', '.test'));

		$this->assertEquals('file', StringHelper::basename('C:\path\to\file'));
		$this->assertEquals('file.test', StringHelper::basename('C:\path\to\file.test', '.test2'));
		$this->assertEquals('file', StringHelper::basename('C:\path\to\file.test', '.test'));
	
		// mixed paths
		$this->assertEquals('file.test', StringHelper::basename('/path\to/file.test'));
		$this->assertEquals('file.test', StringHelper::basename('/path/to\file.test'));
		$this->assertEquals('file.test', StringHelper::basename('\path/to\file.test'));

		// \ and / in suffix
		$this->assertEquals('file', StringHelper::basename('/path/to/filete/st', 'te/st'));
		$this->assertEquals('st', StringHelper::basename('/path/to/filete/st', 'te\st'));
		$this->assertEquals('file', StringHelper::basename('/path/to/filete\st', 'te\st'));
		$this->assertEquals('st', StringHelper::basename('/path/to/filete\st', 'te/st'));

		// http://www.php.net/manual/en/function.basename.php#72254
		$this->assertEquals('foo', StringHelper::basename('/bar/foo/'));
		$this->assertEquals('foo', StringHelper::basename('\\bar\\foo\\'));
	}
	
	public function testByteLength() {
		$this->assertEquals(4, StringHelper::byteLength('this'));
		$this->assertEquals(4, StringHelper::byteLength('año'));
		$this->assertEquals(6, StringHelper::byteLength('это'));
	}
	
	public function testByteSubstr() {
		$this->assertEquals('th', StringHelper::byteSubstr('this', 0, 2));
		$this->assertEquals('э', StringHelper::byteSubstr('это', 0, 2));
		$this->assertEquals('abcdef', StringHelper::byteSubstr('abcdef', 0));
		$this->assertEquals('abcdef', StringHelper::byteSubstr('abcdef', 0, null));
		$this->assertEquals('de',     StringHelper::byteSubstr('abcdef', 3, 2));
		$this->assertEquals('def',    StringHelper::byteSubstr('abcdef', 3));
		$this->assertEquals('def',    StringHelper::byteSubstr('abcdef', 3, null));
		$this->assertEquals('cd',     StringHelper::byteSubstr('abcdef', -4, 2));
		$this->assertEquals('cdef',   StringHelper::byteSubstr('abcdef', -4));
		$this->assertEquals('cdef',   StringHelper::byteSubstr('abcdef', -4, null));
		$this->assertEquals('',   StringHelper::byteSubstr('abcdef', 4, 0));
		$this->assertEquals('',   StringHelper::byteSubstr('abcdef', -4, 0));
		$this->assertEquals('это', StringHelper::byteSubstr('это', 0));
		$this->assertEquals('это', StringHelper::byteSubstr('это', 0, null));
		$this->assertEquals('т',     StringHelper::byteSubstr('это', 2, 2));
		$this->assertEquals('то',    StringHelper::byteSubstr('это', 2));
		$this->assertEquals('то',    StringHelper::byteSubstr('это', 2, null));
		$this->assertEquals('т',     StringHelper::byteSubstr('это', -4, 2));
		$this->assertEquals('то',   StringHelper::byteSubstr('это', -4));
		$this->assertEquals('то',   StringHelper::byteSubstr('это', -4, null));
		$this->assertEquals('',   StringHelper::byteSubstr('это', 4, 0));
		$this->assertEquals('',   StringHelper::byteSubstr('это', -4, 0));
	}
	
	public function testContains() {
		$this->assertTrue(StringHelper::contains($this->_string, 'Uti'));
		$this->assertFalse(StringHelper::contains($this->_string, 'UTI'));
		$this->assertTrue(StringHelper::contains($this->_string, 'UTI', 0, false));
		$this->assertTrue(StringHelper::contains($this->_string, 'UTI', 8, false));
		$this->assertTrue(StringHelper::contains($this->_string, 'Uti', 8));
		$this->assertFalse(StringHelper::contains($this->_string, 'Uti', 10));
	}

	public function testWordCount() {
		$this->assertEquals(3, StringHelper::countWords('china 中国 ㄍㄐㄋㄎㄌ'));
		$this->assertEquals(4, StringHelper::countWords('и много тут слов?'));
		$this->assertEquals(4, StringHelper::countWords("и\rмного\r\nтут\nслов?"));
		$this->assertEquals(1, StringHelper::countWords('крем-брюле'));
		$this->assertEquals(1, StringHelper::countWords(' слово '));
}
	
	public function testEndsWith() {
		// case strict version check
		$this->assertTrue(StringHelper::endsWith($this->_string, 'y'));
		$this->assertTrue(StringHelper::endsWith($this->_string, 'rary'));
		// case unstrict version check
		$this->assertTrue(StringHelper::endsWith($this->_string, 'Y', false));
		$this->assertTrue(StringHelper::endsWith($this->_string, 'RARY', false));
	}
	
	public function testExplode() {
		$this->assertEquals(['It', 'is', 'a first', 'test'], StringHelper::explode("It, is, a first, test"));
		$this->assertEquals(['It', 'is', 'a test with trimmed digits', '0', '1', '2'], StringHelper::explode("It, is, a test with trimmed digits, 0, 1, 2", ',', true, true));
		$this->assertEquals(['It', 'is', 'a second', 'test'], StringHelper::explode("It+ is+ a second+ test", '+'));
		$this->assertEquals(['Save', '', '', 'empty trimmed string'], StringHelper::explode("Save, ,, empty trimmed string", ','));
		$this->assertEquals(['Здесь', 'multibyte', 'строка'], StringHelper::explode("Здесь我 multibyte我 строка", '我'));
		$this->assertEquals(['Disable', '  trim  ', 'here but ignore empty'], StringHelper::explode("Disable,  trim  ,,,here but ignore empty", ',', false, true));
		$this->assertEquals(['It/', ' is?', ' a', ' test with rtrim'], StringHelper::explode("It/, is?, a , test with rtrim", ',', 'rtrim'));
		$this->assertEquals(['It', ' is', ' a ', ' test with closure'], StringHelper::explode("It/, is?, a , test with closure", ',', function ($value) { return trim($value, '/?'); }));
	}
	
	public function testExtract() {
		$this->assertEquals('Ocelote Utility Library', StringHelper::extract($this->_string, 0));
		$this->assertEquals('Ocelote', StringHelper::extract($this->_string, 0, 7));
		$this->assertEquals('Library', StringHelper::extract($this->_string, -7));
		$this->assertEquals('Library', StringHelper::extract($this->_string, -7, 8));
		$this->assertEquals('Utility', StringHelper::extract($this->_string, 8, 7));
	}
	
	public function testIndexOf() {
		$this->assertEquals(0, StringHelper::indexOf($this->_string, 'O'));
		$this->assertEquals(2, StringHelper::indexOf($this->_string, 'e'));
		$this->assertEquals(8, StringHelper::indexOf($this->_string, 'U'));
		// Not found
		$this->assertFalse(StringHelper::indexOf($this->_string, 'x'));
		// Case-insensitive
		$this->assertEquals(0, StringHelper::indexOf($this->_string, 'O', 0, false));
		$this->assertEquals(0, StringHelper::indexOf($this->_string, 'o', 0, false));
		// Offset
		$this->assertEquals(2, StringHelper::indexOf($this->_string, 'e'));
		$this->assertEquals(6, StringHelper::indexOf($this->_string, 'e', 5, true));
	}
	
	public function testStartsWith() {
		$this->assertTrue(StringHelper::startsWith($this->_string, 'Ocelote'));
		$this->assertFalse(StringHelper::startsWith($this->_string, 'Xi'));
		// case-insensitive
		$this->assertTrue(StringHelper::startsWith($this->_string, 'OCELOTE', false));
		$this->assertFalse(StringHelper::startsWith($this->_string, 'XI', false));
	}

	public function testTruncate() {
		$this->assertEquals('привет, я multibyte...', StringHelper::truncate('привет, я multibyte строка!', 20));
		$this->assertEquals('Не трогаем строку', StringHelper::truncate('Не трогаем строку', 20));
		$this->assertEquals('исполь!!!', StringHelper::truncate('используем восклицательные знаки', 6, '!!!'));
	}

	public function testTruncateWords() {
		$this->assertEquals('это тестовая multibyte строка', StringHelper::truncateWords('это тестовая multibyte строка', 5));
		$this->assertEquals('это тестовая multibyte...', StringHelper::truncateWords('это тестовая multibyte строка', 3));
		$this->assertEquals('это тестовая multibyte!!!', StringHelper::truncateWords('это тестовая multibyte строка', 3, '!!!'));
		$this->assertEquals('это строка с          неожиданными...', StringHelper::truncateWords('это строка с          неожиданными пробелами', 4));
	}

	public function dataProviderMbUcFirst() {
		return [
			['foo', 'Foo'],
			['foo bar', 'Foo bar'],
			['👍🏻 foo bar', '👍🏻 foo bar'],
			['', ''],
			[null, ''],
			['здесь我 multibyte我 строка', 'Здесь我 multibyte我 строка'],
		];
	}

	public function testMbUcFirst() {
		$this->assertSame("FooЗдесь我", StringHelper::mbUcFirst("fooЗдесь我"));
	}

	public function dataProviderMbUcWords() {
		return [
			['foo', 'Foo'],
			['foo bar', 'Foo Bar'],
			['👍🏻 foo bar', '👍🏻 Foo Bar'],
			['', ''],
			[null, ''],
			['здесь我 multibyte我 строка', 'Здесь我 Multibyte我 Строка'],
		];
	}

	public function testMbUcWords() {
		$this->assertSame("La Palabra", StringHelper::mbUcWords("la palabra"));
	}
}