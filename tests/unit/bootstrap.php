<?php

// Ensure we get report on all possible php errors
error_reporting(E_ALL | E_STRICT);

define('DS', DIRECTORY_SEPARATOR);
define('TEST_DIR', __DIR__);
define('VENDOR_DIR', TEST_DIR.'/../../vendor');
define('AUTOLOADER_FILE', VENDOR_DIR.'/autoload.php');

if(!file_exists(AUTOLOADER_FILE)){
	exit("Please install the project via composer for autoload function.\n\n");
}

$loader = require AUTOLOADER_FILE;
$loader->addPsr4("michiq\\", TEST_DIR);