<?php

namespace michiq;

use PHPUnit\Framework\TestCase;
use xibalba\ocelote\ArrayHelper;

class ArrayHelperTest extends TestCase {
	public function testGetColumn() {
		$array = [
			'a' => ['id' => '123', 'data' => 'abc'],
			'b' => ['id' => '345', 'data' => 'def'],
		];

		$result = ArrayHelper::getColumn($array, 'id');
		$this->assertEquals(['a' => '123', 'b' => '345'], $result);
		$result = ArrayHelper::getColumn($array, 'id', false);
		$this->assertEquals(['123', '345'], $result);

		$result = ArrayHelper::getColumn($array, function ($element) {
			return $element['data'];
		});

		$this->assertEquals(['a' => 'abc', 'b' => 'def'], $result);
		$result = ArrayHelper::getColumn($array, function ($element) {
			return $element['data'];
		}, false);

		$this->assertEquals(['abc', 'def'], $result);
	}

	public function valueProvider() {
		return [
			['name', 'test'],
			['noname', null],
			['noname', 'test', 'test'],
			['post.id', 5],
			['post.id', 5, 'test'],
			['nopost.id', null],
			['nopost.id', 'test', 'test'],
			['post.author.name', 'human'],
			['post.author.noname', null],
			['post.author.noname', 'test', 'test'],
			['post.author.profile.title', '1666'],
			['admin.firstname', 'Kukulkan'],
			['admin.firstname', 'Kukulkan', 'test'],
			['admin.lastname', 'Ixchel'],
			[
				function ($array, $defaultValue) {
					return $array['date'] . $defaultValue;
				},
				'31-12-2113test',
				'test'
			],
			[['version', '1.0', 'status'], 'released'],
			[['version', '1.0', 'date'], 'defaultValue', 'defaultValue'],
		];
	}

	/**
	 * @dataProvider valueProvider
	 *
	 * @param $key
	 * @param $expected
	 * @param null $default
	 */
	public function testGetValue($key, $expected, $default = null) {
		$array = [
			'name' => 'test',
			'date' => '31-12-2113',
			'post' => [
				'id' => 5,
				'author' => [
					'name' => 'human',
					'profile' => [
						'title' => '1666',
					],
				],
			],
			'admin.firstname' => 'Kukulkan',
			'admin.lastname' => 'Ixchel',
			'admin' => [
				'lastname' => 'human',
			],
			'version' => [
				'1.0' => [
					'status' => 'released'
				]
			],
		];
		$this->assertEquals($expected, ArrayHelper::getValue($array, $key, $default));
	}

	public function testKeyExists() {
		$array = [
			'a' => 1,
			'B' => 2,
		];
		$this->assertTrue(ArrayHelper::keyExists('a', $array));
		$this->assertFalse(ArrayHelper::keyExists('b', $array));
		$this->assertTrue(ArrayHelper::keyExists('B', $array));
		$this->assertFalse(ArrayHelper::keyExists('c', $array));
		$this->assertTrue(ArrayHelper::keyExists('a', $array, false));
		$this->assertTrue(ArrayHelper::keyExists('b', $array, false));
		$this->assertTrue(ArrayHelper::keyExists('B', $array, false));
		$this->assertFalse(ArrayHelper::keyExists('c', $array, false));
	}

	public function testMap() {
		$array = [
			['id' => '123', 'name' => 'aaa', 'class' => 'x'],
			['id' => '124', 'name' => 'bbb', 'class' => 'x'],
			['id' => '345', 'name' => 'ccc', 'class' => 'y'],
		];
		$result = ArrayHelper::map($array, 'id', 'name');
		$this->assertEquals([
			'123' => 'aaa',
			'124' => 'bbb',
			'345' => 'ccc',
		], $result);
		$result = ArrayHelper::map($array, 'id', 'name', 'class');
		$this->assertEquals([
			'x' => [
				'123' => 'aaa',
				'124' => 'bbb',
			],
			'y' => [
				'345' => 'ccc',
			],
		], $result);
	}

	public function testMerge() {
		$a = [
			'name' => 'Xibalba',
			'version' => '1.0',
			'options' => [
				'namespace' => false,
				'unittest' => false,
			],
			'features' => [
				'mvc',
			],
		];
		$b = [
			'version' => '1.1',
			'options' => [
				'unittest' => true,
			],
			'features' => [
				'awesome',
			],
		];
		$c = [
			'version' => '2.0',
			'options' => [
				'namespace' => true,
			],
			'features' => [
				'debug',
			],
		];
		$result = ArrayHelper::merge($a, $b, $c);
		$expected = [
			'name' => 'Xibalba',
			'version' => '2.0',
			'options' => [
				'namespace' => true,
				'unittest' => true,
			],
			'features' => [
				'mvc',
				'awesome',
				'debug',
			],
		];
		$this->assertEquals($expected, $result);
	}

	public function testRemove() {
		// Test with associative
		$array = ['type' => 'A', 'options' => [1, 2]];

		$type = ArrayHelper::remove($array, 'type');

		$this->assertEquals('A', $type);
		$this->assertEquals(['options' => [1, 2]], $array);

		// Test with indexed
		$array = ['water', 'coffee', 'milk', 'beer'];
		$val = ArrayHelper::remove($array, 2);

		$this->assertEquals('milk', $val);
		$this->assertEquals([0, 1, 2], array_keys($array));

		// Test without reindex
		$array = ['water', 'coffee', 'milk', 'beer'];
		$val = ArrayHelper::remove($array, 2, false);

		$this->assertEquals('milk', $val);
		$this->assertEquals([0, 1, 3], array_keys($array));

		// Test defaul
		$array = ['foo', 'bar'];
		$val = ArrayHelper::remove($array, 'baz', false, 'baz');
		$this->assertEquals('baz', $val);
	}

	public function testHasEmptyValues() {
		// Indexed array with empty values
		$indexed = ["", 0, null, false, []];

		// Associative array with empty values
		$associative = ["string" => "", "number" => 0, "null" => null, "bool" => false, "array" => []];

		// An array of empty values return false
		$this->assertFalse(empty($indexed));

		$this->assertTrue(ArrayHelper::hasEmptyValues($indexed));
		$this->assertTrue(ArrayHelper::hasEmptyValues($associative, ["string", "number", "null", "bool", "array"]));

		// Turn false
		$indexed[2] = "no empty";
		$associative["null"] = "no empty";

		$this->assertFalse(ArrayHelper::hasEmptyValues($indexed, [1, 2, 3]));
		$this->assertFalse(ArrayHelper::hasEmptyValues($associative, ["number", "null"]));

		// launch exceptions
		$this->expectException(\InvalidArgumentException::class);

		$this->assertFalse(ArrayHelper::hasEmptyValues($indexed, [9]));
		$this->assertFalse(ArrayHelper::hasEmptyValues($associative, ["exception"]));
		$this->assertFalse(ArrayHelper::hasEmptyValues($indexed, ["foo" => "bar"]));
	}
}
