<?php

namespace michiq;

use PHPUnit\Framework\TestCase;

use xibalba\ocelote\Checker;
use xibalba\ocelote\Converter;

/**
 * Tests for Converter.
 * Also test Checker.
 */
class ConverterTest extends TestCase {
	public function testIs() {
		$balam = ['yeye' => 'balam'];
		$objBalam = Converter::toObject($balam);
		$xml = Converter::toXml($balam);
		$objXml = new \SimpleXMLElement($xml);
		
		$this->assertEquals('array', Checker::is($balam));
		$this->assertEquals('array', Checker::is(Converter::toArray($objBalam)));
		$this->assertEquals('array', Checker::is(Converter::xmlToArray($objXml)));
		$this->assertEquals('object', Checker::is($objBalam));
		$this->assertEquals('json', Checker::is(Converter::toJson($balam)));
		$this->assertEquals('serialized', Checker::is(Converter::toSerialize($balam)));
		$this->assertEquals('xml', Checker::is($xml));
		$this->assertEquals('string', Checker::is('string'));
		$this->assertEquals('integer', Checker::is(6));
		$this->assertEquals('double', Checker::is(6.6));
		$this->assertEquals('boolean', Checker::is(true));
		
		$this->assertTrue(Checker::isAssociative($balam));
		
		$balam['kinich'] = 'yax';
		$balam[2] = 'mo';
		
		$this->assertTrue(Checker::isAssociative($balam));
		$this->assertFalse(Checker::isAssociative($balam, true));
		$this->assertTrue(Checker::isIndexed(['mayan', 'inca']));
		$this->assertFalse(Checker::isIndexed($balam));
	}
	
	public function testAutobox() {
		$this->assertEquals(6, Converter::autobox('6'));
		$this->assertEquals(6.6, Converter::autobox('6.6'));
		$this->assertEquals('6x', Converter::autobox('6x'));
		$this->assertTrue(Converter::autobox('true'));
		$this->assertFalse(Converter::autobox('false'));
	}
	
	public function testUnbox() {
		$this->assertEquals('6', Converter::unbox(6));
		$this->assertEquals('true', Converter::unbox(true));
		$this->assertEquals('false', Converter::unbox(false));
	}
}